
exports.up = function(knex, Promise) {
    return knex.schema 
    .createTable ('users', function (table) { 
      table.increments('id'). primary (); 
      table.string ('firstName'); 
      table.string ('lastName'); 
      table.string ('emailAddress');
      table.string('password_digest');
      table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
      table.timestamp('updated_at');

    })

    .createTable('categorys',function(table){
            table.increments('id').primary();
            table.string('name');
        })
    
    .createTable('posts',function(table){
        table.increments('id').primary();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('id').inTable('users');
        table.integer('category_id').unsigned();
        table.foreign('category_id').references('id').inTable('categorys')
        table.string('title');
        table.text('description');
        table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
        table.timestamp('updated_at');
      })

     
};



exports.down = function(knex, Promise) {
    return knex.schema 
    .dropTable('categorys')
    .dropTable('posts') 
    .dropTable ('users')
};
