
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('comments',function(table){
        table.increments('id').primary();
        table.text('description');
        table.integer('id_user').unsigned();
        table.foreign('id_user').references('id').inTable('users');
        table.integer('id_post').unsigned();
        table.foreign('id_post').references('id').inTable('posts');
        table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
        table.timestamp('updated_at');
    });
};

exports.down = function(knex, Promise) {
    // return knex.schema
    // .dropTable('comments')
};
