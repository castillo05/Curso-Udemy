'use strict';
var knex = require('knex')(require('./knexfile'));
var bookshelf = require('bookshelf')(knex);
var securePassword=require('bookshelf-secure-password');
module.exports = bookshelf,bookshelf.plugin(securePassword);