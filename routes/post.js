var express=require('express');

var md_auth=require('../middelware/authenticated');

var PostController=require('../controllers/post');
var api=express.Router();

api.get('/pruebapost',PostController.pruebapost);

api.post('/createpost',PostController.createPost);

api.get('/allpost',PostController.allPost);

api.get('/getpost/:id',PostController.getPost);
module.exports=api;