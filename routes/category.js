'use strict'

var express=require('express');

var md_auth=require('../middelware/authenticated');

var CategoryController=require('../controllers/category');

var api=express.Router();

api.post('/categorycreate',CategoryController.newCategory);

module.exports=api;