var express = require('express');

var md_auth=require('../middelware/authenticated');

var UserController=require('../controllers/user');
var api=express.Router();
api.get('/usertest/:emailAddress',UserController.prueba);
api.get('/users',UserController.UserList);
api.post('/usercreate',md_auth.ensureAuth,UserController.SaveUser);
api.put('/updateuser/:id',md_auth.ensureAuth,UserController.UpdateUser);
api.delete('/deleteuser/:id',UserController.DeleteUser);
api.post('/signin',UserController.SignIn);
api.get('/viewPost/:id',UserController.viewPosts);

// api.get('/users',function(err,res){
//   if(err){ return console.log(err)}
//   res.status(200).send({message:User.fetchAll.json});
// });

// var router = express.Router();
// router.route('/')
//   .get(function(req, res) {
//     User
//       .fetchAll()
//       .then(function(users) {
//         res.json({ users });
//       });
//   });
module.exports = api;