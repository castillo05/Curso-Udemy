'use strict'

var bookshelf=require('../bookshelf');
var User = require('../models/user');
var Post = require('../models/post');

var Comment=bookshelf.Model.extend({
    tableName:'comments',
    hasTimestamps:true,

    user:function(){
        return this.belongsTo(User);
    },

    post:function(){
        return this.belongsTo(Post);
    }
});

module.exports=Comment;