var bookshelf=require('../bookshelf');
var Post=require('./post');
var Comment=require('./comment');

var User= bookshelf.Model.extend({
   
    tableName:'users',
    hasTimestamps:true,
    hasSecurePassword:true,

    post:function(){
        return this.hasMany(Post);
    },

    comment:function(){
        return this.hasMany(Comment);
    }

});

module.exports=User;