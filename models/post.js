var bookshelf=require('../bookshelf');
var User=require('./user');
var Category=require('./category');
var Comment=require('./comment');

var Post= bookshelf.Model.extend({
   
    tableName:'posts',
    hasTimestamps:true,

    user:function(){
        return this.belongsTo(User);
    },

    category:function () {
        return this.belongsTo(Category);
    },

    coment:function(){
        return this.hasMany(Comment);
    }

});

module.exports=Post;