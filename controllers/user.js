'use strict'

var User = require('../models/user');
var jwt=require('../service/jwt');
// Metodo de pruebas
function prueba(req,res) {
    // res.status(200).send({message:'Hola desde Controlador User'});
    new User({emailAddress:req.params.emailAddress}).fetch().then(function(users){
        res.status(200).send({users:users.get('emailAddress')});
    });
}

// Metodo para Listar Usuarios
function UserList(req,res){
   User.fetchAll().then(function(users){
        res.status(200).send(users);
    });    
    
}

// Metodo para Crear usuarios
function SaveUser(req,res){

    // Extraemos los usuarios y comparamos los email *Si son iguales manda el siguiente mensaje message:'Este Nombre ya existe'*
       User.where('emailAddress',req.body.emailAddress)
       .fetch()
       .then(function(users){
        if (users==null) {
            
            new User({
            firstName:req.body.firstName,
            lastName:req.body.lastName,
            emailAddress:req.body.emailAddress,
            password:req.body.password
            }).save()
            .then(function(user){
                res.status(200).send({user:user})
            });
        }else{
            return res.status(404).send({message:'Este Email ya existe'});
        }
    }).catch(function(err){
        res.json('err');
    }); 
        
   
}
// Metodo para Actualizar un Usuario
function UpdateUser(req,res){
   

    User.where('id',req.params.id)
    .fetch()
    .then(function(users){
        if (users==null) {
           return res.status(404).send({message:'Este Usuario no existe'});
        }else{
            User.where('id',req.params.id).fetch().then(function(user){
                user.save({
                    firstName:req.body.firstName,
                    lastName:req.body.lastName,
                    emailAddress:req.body.emailAddress,
                }).then(function(saved){
                    if(saved){
                    res.status(200).send({user:saved}); 
                    }
                    
                });
            });
        }
    }); 

     
}
// Eliminar un usuario
function DeleteUser(req,res){

    User.where('id',req.params.id).fetch().then(function(user){
        if (user==null) {
            res.status(404).send({message:'Este usuario no existe en la Base de Datos'});
        }else{
             new User({id:req.params.id})
            .destroy()
            .then(function(user){
                res.status(200).send({user:'Usuario Eliminado'});
            });
        }
    });

   

   
}
 function SignIn(req,res){
    

        User.forge({emailAddress:req.body.emailAddress})
        .fetch()
        .then(function(user){
            if (user==null) {
                res.status(200).send({Message:'Este Usuario no existe'});
            } else {
                user.authenticate(req.body.password).then(function(u){
                    if (req.body.hash) {
                        res.status(200).send({token:jwt.createToken(u)});
                    }else{
                        res.status(200).send({user:u});
                    }

                   
                    
                },function(err){
                    res.status(404).send({message:err});
                });
            }
            
        });
    }

    // Mostrar los post de un usuario

    function viewPosts(req,res){
        new User({'id':req.params.id}).fetch({withRelated:[
            'post',
            {post:function(query){query.orderBy('created_at');}}

        ]
    }).then(function(user){
        res.status(200).send(user.related('post'));
    }).catch(function(err){
        res.status(505).send({err});
    });
    }
// Exportamos como modulos para Usuarlos en cualquier parte del codigo
module.exports={
    prueba,
    UserList,
    SaveUser,
    UpdateUser,
    DeleteUser,
    SignIn,
    viewPosts
}