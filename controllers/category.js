'use strict'

var category = require('../models/category');

function newCategory(req,res){
    new category({
        name:req.body.name
    }).save().then(function(category){
        res.status(200).send({category:category});
    }).catch(function(err){
        res.status(505).send({error:err});
    });
}

module.exports={
    newCategory
}