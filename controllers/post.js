'use strict'

var User = require('../models/user');
var Post = require('../models/post');

function pruebapost(req,res){
    return res.status(200).send({message:'Hola desde Controlador post'});


}

function createPost(req,res){
    new Post({
        user_id:req.body.user_id,
        title:req.body.title,
        description:req.body.description
    }).save().then(function(p){
        res.status(200).send({post:p});
    }).catch(function(err){
        res.json(err);
    });
}

function allPost(req,res){
    Post.forge().fetch().then(function(collection){
        res.status(200).send({post:collection});
    }).catch(function(err){
        res.status(500).send({error:true,data:{message:err.message}});
    });
}

function getPost(req,res){
    Post.where('id',req.params.id).fetch().then(function(post){
        User.where('id',post.get('user_id')).fetch().then(function(user){
            res.status(200).send({user:user,post:post});
        });
      
    }).catch(function(err){
        res.status(505).send({error:err});
    });

    
}

module.exports={
    pruebapost,
    createPost,
    allPost,
    getPost
}