'use strict'
var jwt= require('jwt-simple');
var moment= require('moment');
var secret='clave_secreta';

exports.createToken=function (u) {
    var payload={
        lastName:'lastName',
        emailAddress:'emailAddress',
        iat:moment().unix(),
        exp:moment().add(30,'days').unix
    };

   return jwt.encode(payload,secret);
}