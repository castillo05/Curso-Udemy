var express = require('express');

var bodyParser=require('body-parser');
var userRoute = require('./routes/user');
var postRoute=require('./routes/post');
var app=express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// Configurar las cabeceras
// app.use((req,res,next)=>{
//     res.header('Access-Control-Allow-Origin','*');
// 	res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY,Origin,X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request');
// 	res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
// 	res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');

// });


app.use('/api',userRoute);
app.use('/api',postRoute);

app.get('/prueba',function(req,res){
    res.status(200).send({mmesage:'Hola Mundo'});
});

module.exports=app;