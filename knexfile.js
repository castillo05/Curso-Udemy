'use strict';
module.exports = {
    client:'mysql',
    connection:{
        host:'localhost',
        user:'root',
        port:3306,
        password:'',
        database:'knex',
        charset:'utf8'
    },
  useNullAsDefault: true,
};